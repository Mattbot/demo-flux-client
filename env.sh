#!/bin/bash

# Developer-oriented environment setup script
# -------------------------------------------
#
# Source this in your shell for the correct environment variables
#
# If you add a variable here, please check if it's set already and ignore it in that case

export REACT_APP_POST_API_BASE_URL='http://0.0.0.0:34255'
export REACT_APP_USER_API_BASE_URL='http://0.0.0.0:34254'
export COMPOSE_PROJECT_NAME="${COMPOSE_PROJECT_NAME:-demorefluxwww}"

