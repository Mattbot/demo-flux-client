FROM quay.io/mattbot/base:ubuntu-18.04-4

RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - \
  && echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list

RUN curl -sL https://deb.nodesource.com/setup_10.x -o /tmp/nodesource_setup.sh \
  && bash /tmp/nodesource_setup.sh

RUN apt-get update \
  && apt-get install -y --no-install-recommends \
    build-essential \
    nodejs \
    yarn \
  && rm -rf /var/lib/apt/lists/*

WORKDIR /app/

# Provide *default* values for some of our configuration variables:
ENV \
  REACT_APP_POST_API_BASE_URL='http://0.0.0.0:34255' \
  REACT_APP_USER_API_BASE_URL='http://0.0.0.0:34254'

COPY . /app/

RUN npm install

ENTRYPOINT ["/sbin/tini", "-vvg", "--", "/app/docker/entrypoint.sh"]

EXPOSE 3003

STOPSIGNAL SIGINT

CMD ["yarn", "start"]
