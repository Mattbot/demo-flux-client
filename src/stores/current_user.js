import Reflux from 'reflux';
import Immutable from 'immutable';
import JWT from 'jwt-js';

import History from '../lib/history';
import AppStore from '../stores/app';
import CurrentUserActions from '../actions/current_user';

export default class CurrentUserStore extends Reflux.Store
{
  constructor () {
      super();
      this.listenables = [CurrentUserActions];
  }

  init () {
    AppStore.cursorFor('current_user');
  }

  emit () {
    return this.state;
  }

  getInitialState () {
    return this.state || Immutable.Map();
  }

  onSignInCompleted (data) {
    const current_user = this.buildCurrentUser(data)
    this.setState({ current_user: current_user });
    History.goBack() || History.push('/');
  }

  onSignInFailed (message) {
    console.error('ERROR > User sign in failed!: ', message);
  }

  onSignOut () {
    this.setState({ current_user: null });
  }

  buildCurrentUser (api_response) {
    const token_data = JWT.decodeToken(api_response.jwt);
    return {
      id: token_data.payload.user_id,
      email: token_data.payload.email,
      username: token_data.payload.username,
      token: api_response.jwt
    }
  }
}
