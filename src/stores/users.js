import Reflux from 'reflux';
import Immutable from 'immutable';

import AppStore from '../stores/app';
import UserActions from '../actions/user';

export default class UsersStore extends Reflux.Store
{
  constructor () {
      super();
      this.listenables = [UserActions];
      this.state = { users: Immutable.List() };
  }

  init () {
    AppStore.cursorFor('users');
  }

  emit () {
    return this.state.deref();
  }

  getInitialState () {
    return this.state || { users: Immutable.List() };
  }

  setState (newState) {
    this.state = newState;
    this.trigger(this.state);
  }

  onAllUserCompleted (data) {
    let newList = this.state.users;
    data.forEach ( (datum) => {
      newList = newList.set(datum.id, datum);
    });
    this.setState({ users: newList});
  }

  onFindUserCompleted (data) {
    let newList = this.state.users;
    newList = newList.set(data.id, data);
    this.setState({ users: newList});
  }
}
