import Reflux from 'reflux';
import Immutable from 'immutable';
import Cursor from 'immutable-cursor';

export default class AppStore extends Reflux.Store
{
  init () {
    console.log('in app store init');
    this.state = Immutable.Map(
      {
        stores: {
          current_post: Immutable.Map(),
          current_user: Immutable.Map(),
          notice: Immutable.Map(),
          users: Immutable.List(),
          posts: Immutable.OrderedMap(),
        }
      }
    );
  }

  setState (newState) {
    this.state = newState;
    this.trigger(this.state);
  }

  pathForStore (storeName) {
    switch (storeName) {
      case 'current_post':
        return ['stores', 'current_post'];
      case 'current_user':
        return ['stores', 'current_user'];
      case 'notice':
        return ['stores', 'notice'];
      case 'users':
        return ['stores', 'users'];
      case 'posts':
        return ['stores', 'posts'];
      default:
        return false;
    }
  }

  cursorFor (storeName) {
    return Cursor.from(this.state, this.pathForStore(storeName), this.setState());
  }
}
