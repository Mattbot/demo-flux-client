import Reflux from 'reflux';
import Immutable from 'immutable';

import AppStore from '../stores/app';
import CurrentUserActions from '../actions/current_user';
import NoticeActions from '../actions/notice';


export default class NoticeStore extends Reflux.Store
{
  constructor () {
      super();
      this.listenables = [CurrentUserActions, NoticeActions];
      this.state = { notice: Immutable.Map({}), show: false};
  }

  init () {
    AppStore.cursorFor('notice');
  }

  emit () {
    return this.state.deref();
  }

  getInitialState () {
    return this.state || { notice: Immutable.Map() };
  }

  setState (newState) {
    this.state = newState;
    this.trigger(this.state);
  }

  onSignInFailed (error) {
    const message = 'Sign In Failed: ';
    this.setState({ notice: message + JSON.stringify(error), show: true });
  }

  onDisplayNotice (message) {
    this.setState({ notice: message, show: true });
  }
}
