import Reflux from 'reflux';
import Immutable from 'immutable';

import History from '../lib/history';
import AppStore from '../stores/app';
import CurrentPostActions from '../actions/current_post';
import PostActions from '../actions/post';
import UserActions from '../actions/user';

export default class CurrentPostStore extends Reflux.Store
{
  constructor () {
      super();
      this.listenables = [CurrentPostActions, PostActions, UserActions];
  }

  init () {
    AppStore.cursorFor('current_post');
  }

  emit () {
    return this.state;
  }

  getInitialState () {
    return this.state || Immutable.Map();
  }

  onDeletePostCompleted () {
    this.setState({ current_post: Immutable.Map() });
    History.push('/posts');
  }

  onGetPostCompleted (api_response) {
    this.updateCurrentPost(api_response);
  }

  onFindPostCompleted (api_response) {
    this.updateCurrentPost(api_response);
    CurrentPostActions.getUsername(this.state.current_post.get('user_id'));
  }

  onFindUserCompleted (api_response) {
    let current_post = this.state.current_post;
    current_post = current_post.set('username', api_response.username || api_response.email || api_response.id);
    this.setState({ current_post: current_post });
  }

  onEditPostCompleted (api_response) {
    this.updateCurrentPost(api_response);
  }

  onNewPostCompleted (api_response) {
    this.updateCurrentPost(api_response);
  }

  updateCurrentPost (data) {
    let current_post = this.buildCurrentPost(data);
    this.setState({ current_post: Immutable.Map(current_post) });
  }

  buildCurrentPost (data) {
    return(Immutable.Map({
      id: data.id,
      user_id: data.user_id,
      username: data.username || null,
      title: data.title,
      body: data.body,
      created_at: data.created_at,
      updated_at: data.updated_at,
    }));
  }
}
