import _ from 'lodash';
import Reflux from 'reflux';
import Immutable from 'immutable';

import AppStore from '../stores/app';
import History from '../lib/history';
import PostActions from '../actions/post';

export default class PostsStore extends Reflux.Store
{
  constructor () {
      super();
      this.listenables = [PostActions];
      this.state = { posts: Immutable.OrderedMap() };
  }

  init () {
    AppStore.cursorFor('posts');
  }

  emit () {
    return this.state.deref();
  }

  getInitialState () {
    return this.state || { posts: Immutable.OrderedMap() };
  }

  setState (newState) {
    this.state = newState;
    this.trigger(this.state);
  }

  onAllPostCompleted (data) {
    const newPosts = _.keyBy(data, 'id');
    this.setState({ posts: Immutable.OrderedMap(newPosts) });
  }

  onFindPostCompleted (data) {
    const foundPost = _.keyBy([data], 'id');
    const posts = this.state.posts.set(foundPost.id, foundPost);
    this.setState({ posts: Immutable.OrderedMap(posts) });
  }

  onNewPostCompleted (data) {
    const newPost = _.keyBy([data], 'id');
    const posts = this.state.posts.set(newPost.id, newPost);
    this.setState({ posts: Immutable.OrderedMap(posts) });
    History.push('/posts/' + data.id);
  }

  onEditPostCompleted (data) {
    const editedPost = _.keyBy([data], 'id');
    const posts = this.state.posts.set(editedPost.id, editedPost);
    this.setState({ posts: Immutable.OrderedMap(posts) });
    History.push('/posts/' + data.id);
  }

  onDeletedPostCompleted (data) {
    History.push('/posts');
  }
}
