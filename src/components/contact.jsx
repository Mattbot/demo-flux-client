import React from 'react';

const Contact = () => (
  <div className='contact'>
    <p className="intro">
      By <a href="http://gitlab.com/mattbot">Matt Ridenour</a> 2018.
    </p>
  </div>
);

export default Contact;
