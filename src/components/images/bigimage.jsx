// Libraries:
import React, { Component } from 'react';
// Assets:
import './images.css';

export default class Home extends Component {
  constructor(props) {
    super(props)
    this.image = props.image
    this.alt = props.alt
  }

  render() {
    return (
      <div className='big_image'>
        <img className='big_image_image' src={this.image} alt={this.alt}/>
      </div>
    );
  }
}
