import React from 'react';
import Reflux from 'reflux';
import { Button, ControlLabel, FormControl, FormGroup } from 'react-bootstrap';

import CurrentUserActions from '../../actions/current_user';


export default class SignInForm extends Reflux.Component {
  constructor(props) {
    super(props);
    this.state = { submit: 'submit' };
    this.listenables = [CurrentUserActions];
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  handleSubmit(event) {
    event.preventDefault();
    CurrentUserActions.signIn.triggerAsync(this.state.email, this.state.password);
  }

  render() {
    return (
      <div>
        <form onSubmit={this.handleSubmit}>
          <h1>Please sign in</h1>
          <FormGroup>
            <ControlLabel>Email:</ControlLabel>
            <FormControl
              name='email'
              type="text"
              autoComplete ='username email'
              value={this.state.email}
              placeholder="Enter text"
              onChange={this.handleChange}
            />
            <FormControl.Feedback />
          </FormGroup>
          <FormGroup>
            <ControlLabel>Password:</ControlLabel>
            <FormControl
              name='password'
              type="password"
              autoComplete='current-password'
              value={this.state.password}
              placeholder="Enter text"
              onChange={this.handleChange}
            />
            <FormControl.Feedback />
          </FormGroup>
          <Button type='submit'>Sign In</Button>
        </form>
      </div>
    );
  }
}
