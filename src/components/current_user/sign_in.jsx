import React, { Component } from 'react';
import SignInForm from './_form';

export default class SignIn extends Component {

  onSignInCompleted () {
    this.props.history.push('/');
  }

  render() {
    return (
      <div>
        <SignInForm/>
      </div>
    );
  }
}
