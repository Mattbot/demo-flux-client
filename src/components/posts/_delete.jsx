import React from 'react';
import { Button, Modal } from 'react-bootstrap';

import PostActions from '../../actions/post';

export default class ConfirmDeleteModal extends React.Component {
  constructor (props) {
    super(props);
    this.deletePost = this.deletePost.bind(this);
    this.state = {
      current_post: this.props.current_post,
      current_user: this.props.current_user
    };
  }

  deletePost () {
    PostActions.deletePost(this.state.current_user, this.state.current_post);
    this.props.onClose();
  }

  render() {
    if (this.props.show) {
      return (
        <Modal.Dialog onClose={this.props.onClose}>
          <Modal.Header>
            <Modal.Title>Delete Post? Are you sure?</Modal.Title>
          </Modal.Header>
          <Modal.Footer>
            <Button bsStyle="danger" onClick={this.deletePost}>Delete</Button>
            <Button bsStyle="primary" onClick={this.props.onClose}>Cancel</Button>
          </Modal.Footer>
        </Modal.Dialog>
      );
    } else {
      return null;
    }
  }
}
