import Immutable from 'immutable';
import React from 'react';
import Reflux from 'reflux';

import CurrentUserStore from '../../stores/current_user';
import PostForm from './_form';

export default class NewPost extends Reflux.Component {
  constructor(props) {
    super(props);
    this.stores = [CurrentUserStore];
    this.storeKeys = ['current_user'];
    this.state = {
      current_post: Immutable.Map({title: '', body: ''})
    }
  }

  render () {
    const current_post = this.state.current_post;
    const current_user = this.state.current_user;
    if (!!current_user) {
      return(<PostForm current_post={current_post} current_user={current_user} action='new'/>);
    } else {
      return(<p>Please sign in first.</p>);
    }
  }
}
