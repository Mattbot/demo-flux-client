import _ from 'lodash';

import React from 'react';
import Reflux from 'reflux';

import CurrentUserStore from '../../stores/current_user';

import PostActions from '../../actions/post';
import PostsStore from '../../stores/posts';
import PostSummary from './_summary';

import UserActions from '../../actions/user';
import UsersStore from '../../stores/users';

// import Post from './post';
import { Link } from 'react-router-dom';

export default class Posts extends Reflux.Component {
  constructor (props) {
    super(props);
    this.stores = [CurrentUserStore, PostsStore, UsersStore];
    this.storeKeys = ['current_user', 'posts', 'users'];
  }

  async componentDidMount() {
    try {
      await PostActions.allPost();
      await UserActions.allUser();
    } catch (error) {
      console.error('ERROR: ', error);
    }
  }

  summarizePosts (posts) {
    console.log('posts: ', posts);
    const postWithUserName = posts.map( (post) =>
      this.includeUsername(post)
    );

    return postWithUserName.map( (post) =>
      <div>
        <PostSummary post={post}/>
      </div>
    );

  }

  includeUsername (post) {
    return _.extend(post, {username: this.findUserName(post.user_id)});
  }

  findUserName (id) {
    this.fetchUser(id);
    const user = this.state.users.get(id) || { username: "Unknown" };
    return user.username || user.email || id;
  }

  fetchUser (id) {
    if (!this.state.users.get(id) && !!id) { UserActions.findUser(id) };
  }

  getPosts () {
    return (!!this.state && !!this.state.posts) ? Array.from(this.state.posts) : false;
  }

  newPostLink () {
    return (
      <div className='post-new-link'>
        <Link to="/post/new">Create Post</Link>
      </div>
    );
  }

  render () {
    console.log('index state: ', this.state);
    const posts = this.state.posts;
    return(
      <div className='posts'>
        { !!posts ? this.summarizePosts(posts) : <p>No Posts</p> }
      </div>
    );
  }
}
