import React from 'react';
import Reflux from 'reflux';
import { Link } from 'react-router-dom';
import { Media, Col } from 'react-bootstrap';

import LinesEllipsis from 'react-lines-ellipsis';

export default class PostSummary extends Reflux.Component {
  constructor (props) {
    super(props);
    this.state = this.props.post;
  }

  render () {
    const post = this.state;
    console.log('summary post: ', post);
    return(
      <div className='post'>
        <Media>
          <Media.Body>
            <Media.Heading>
              <Col>
                <Link className='title' to={{ pathname: '/post/' + post.id }}>
                  {post.title}
                </Link>
              </Col>
              <Col>
                Author: {post.username} Date: {post.created_at}
              </Col>
            </Media.Heading>
            <Col>
              <LinesEllipsis
                text={post.body}
                maxLine='3'
                ellipsis='...'
                trimRight
                basedOn='letters'
              />
            </Col>
          </Media.Body>
        </Media>
      </div>
    );
  }
}
