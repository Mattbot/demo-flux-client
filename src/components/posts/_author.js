import _ from 'lodash';
import React from 'react';
import { Link } from 'react-router-dom';

import Role from '../../lib/role';

import ConfirmDeleteModal from './_delete';

export default class AuthorLinks extends React.Component {
  constructor (props) {
    super(props);
    this.handleDelete = this.handleDelete.bind(this);
    this.toggleModal = this.toggleModal.bind(this);
    this.state = {
      current_post: this.props.current_post,
      current_user: this.props.current_user,
      showAlert: false
    };
  }

  handleDelete (event) {
    event.preventDefault();
    this.toggleModal(event);
  }

  toggleModal () {
    const newState = _.extend(this.state, {showAlert: !this.state.showAlert});
    this.setState(newState);
  }

  render () {
    const current_post = this.state.current_post;
    const current_user = this.state.current_user;
    if (Role.isAuthor(current_user, current_post)) {
      return (
        <div className='links'>
          <Link className='link' to={ { pathname: '/post/edit/' + current_post.get('id') } }>Edit</Link>
          <Link className='link' onClick={this.handleDelete} to='#'>Delete</Link>
          <ConfirmDeleteModal
            className='modal'
            show={this.state.showAlert}
            onClose={this.toggleModal}
            current_user={current_user}
            current_post={current_post}
          />
        </div>
      );
    } else {
      return(null);
    }
  }
}
