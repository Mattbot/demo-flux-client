import React from 'react';
import Reflux from 'reflux';

import History from '../../lib/history';
import CurrentUserStore from '../../stores/current_user';
import PostActions from '../../actions/post';
import CurrentPostStore from '../../stores/current_post';
import PostForm from './_form';

export default class EditPost extends Reflux.Component {
  constructor(props) {
    super(props);
    this.state = { id: this.props.match.params['id'], title: null , body: null };
    this.stores = [CurrentUserStore, CurrentPostStore];
    this.storeKeys = ['current_user', 'current_post'];
    this.id = this.props.match.params['id']
  }

  async componentDidMount() {
    try {
     await PostActions.findPost(this.id);
    } catch (error) {
      console.error('ERROR: ', error);
    }
  }

  find (id) {
    return this.state.posts.get(id);
  }

  createForm (current_post, current_user) {
    return(<PostForm current_post={current_post} current_user={current_user} action='edit'/>);
  }

  dontCreateForm () {
    return(<p>Can't create form.</p>);
  }

  navigateSignIn () {
    History.push('/sign_in');
  }

  render () {
    const current_post = this.state.current_post;
    const current_user = this.state.current_user;
    console.log('this.state.current_user: ', current_user);
    console.log('this.state.current_post: ', current_post);
    if (!!current_user) {
      return (!!current_post ? this.createForm(current_post, current_user) :this.dontCreateForm());
    } else {
      return null;
    }
  }
}
