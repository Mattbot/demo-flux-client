import _ from 'lodash';
import Immutable from 'immutable';
import React from 'react';
import Reflux from 'reflux';
import { Button, ControlLabel, FormControl, FormGroup } from 'react-bootstrap';

import PostActions from '../../actions/post';
import NoticeActions from '../../actions/notice';

export default class PostForm extends Reflux.Component {
  constructor(props) {
    super(props);
    this.state = {
      action: this.props.action,
      current_user: this.props.current_user,
      current_post: this.props.current_post || Immutable.Map({title: '', body: ''})
    }
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    const newState = this.state.current_post.set(name, value);
    this.setState(_.extend(this.state, { current_post: newState }));
  }

  handleSubmit(event) {
    event.preventDefault();
    if(this.isSignedIn()) {
      switch (this.state.action) {
        case 'new':
          return this.actionNew();
        case 'edit':
          return this.actionEdit();
        default:
          return false;
      }
    } else {
      this.unauthorized()
    }
  }

  actionNew () {
    PostActions.newPost.triggerAsync(
      this.state.current_user,
      { title: this.state.current_post.get('title'), body: this.state.current_post.get('body') }
    );
  }

  actionEdit () {
    PostActions.editPost.triggerAsync(
      this.state.current_user,
      { id: this.state.current_post.get('id'), title: this.state.current_post.get('title'), body: this.state.current_post.get('body') }
    );
  }

  isSignedIn () {
    return !!this.state.current_user ? true : false;
  }

  unauthorized () {
    console.log('unauthorized!: ', this.state);
    NoticeActions.displayNotice('You are unauthorized to create, edit, or delete posts!');
  }

  render() {
    return (
      <div className='post_form'>
        <form onSubmit={this.handleSubmit}>
          <h2>{_.capitalize(this.state.action)} Post</h2>
          <FormGroup controlId="formBasicText">
            <ControlLabel>Title:</ControlLabel>
            <FormControl
              type="text"
              name='title'
              autoComplete ='title'
              value={this.state.current_post.get('title')}
              onChange={this.handleChange}
            />
            <FormControl.Feedback />
          </FormGroup>
          <FormGroup controlId="formBasicText">
            <br/>
            <ControlLabel>Body:</ControlLabel>
              <FormControl
                className="post_textarea"
                componentClass="textarea"
                type='textarea'
                name='body'
                autoComplete='text'
                value={this.state.current_post.get('body') }
                onChange={this.handleChange}
              />
              <FormControl.Feedback />
            <br/>
          </FormGroup>
          <Button type='submit'>{_.capitalize(this.state.action)}</Button>
        </form>
      </div>
    );
  }
}
