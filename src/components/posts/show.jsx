import React from 'react';
import Reflux from 'reflux';
import { Link } from 'react-router-dom';
import { Media, Col } from 'react-bootstrap';

import CurrentPostStore from '../../stores/current_post';
import CurrentUserStore from '../../stores/current_user';

import PostActions from '../../actions/post';

import AuthorLinks from './_author';
import FourZeroFour from '../app/404';

export default class Post extends Reflux.Component {
  constructor (props) {
    super(props);
    this.stores = [CurrentPostStore, CurrentUserStore];
    this.storeKeys = ['current_post', 'current_user'];
    this.id = this.props.match.params['id'];
    this.state = {};
  }

  async componentDidMount () {
    try {
      await PostActions.findPost(this.id);
    } catch (error) {
      console.error('ERROR: ', error);
    }
  }

  showPost (current_post, current_user) {
    if (!!current_post) {
      return(
        <div className='post'>
          <Media>
            <Media.Body>
              <Media.Heading>
                <Col className='title'>
                  <Link to={{ pathname: '/post/' + current_post.get('id') }}>
                    {current_post.get('title')}
                  </Link>
                </Col>
                <Col className='metadata'>
                  Author: {current_post.get('username')} Date: {current_post.get('created_at')}
                </Col>
                <Col className='controls'>
                  <AuthorLinks current_post={current_post} current_user={current_user} />
                </Col>
              </Media.Heading>
              <Col>
                {current_post.get('body')}
              </Col>
            </Media.Body>
          </Media>
        </div>
      );
    } else {
      return(<FourZeroFour />);
    }
  }

  render () {
    const current_post = this.state.current_post;
    const current_user = this.state.current_user;
    return this.showPost(current_post, current_user);
  }
}
