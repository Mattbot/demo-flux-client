import React from 'react';
import { Link } from 'react-router-dom';

const Footer = () => (
  <div className='footer'>
    <nav>
      <Link to="/contact">Contact</Link>
    </nav>
  </div>
);

export default Footer;
