import React from 'react';
import { Button, Modal } from 'react-bootstrap';

export default class NoticeModal extends React.Component {
  render() {
    if (this.props.show) {
      return (
        <Modal.Dialog onClose={this.props.onClose}>
          <Modal.Header>
            <Modal.Title bsStyle="danger">{this.props.notice}</Modal.Title>
          </Modal.Header>
          <Modal.Footer>
            <Button bsStyle="primary" onClick={this.props.onClose}>OK</Button>
          </Modal.Footer>
        </Modal.Dialog>
      );
    } else {
      return null;
    }
  }
}
