// External Libraries:
import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { Grid } from 'react-bootstrap';

// Components:
import Home from '../home';
import Contact from '../contact';
import SignIn from '../current_user/sign_in';
import Posts from '../posts/index';
import NewPost from '../posts/new';
import EditPost from '../posts/edit';
import Post from '../posts/show';

const App = () => (
  <div className='body'>
    <Route exact path="/" component={Home}/>
    <Route exact path="/post/edit/:id" component={EditPost}/>
    <Route exact path="/post/new" component={NewPost}/>
    <Route exact path="/posts" component={Posts}/>
    <Route exact path="/post/:id" component={Post}/>
    <Route exact path="/contact" component={Contact}/>
    <Route exact path="/sign_in" component={SignIn}/>
  </div>
);

export default App
