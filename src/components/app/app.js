import React from 'react';
import Header from './header';
import Body from './body';
import Footer from './footer';
import '../../stylesheets/app.css';

const App = () => (
  <div className="app">
    <Header/>
    <Body/>
    <Footer/>
  </div>
);

export default App;
