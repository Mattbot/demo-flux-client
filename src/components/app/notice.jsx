import _ from 'lodash';
import Reflux from 'reflux';
import React from 'react';

import NoticeStore from '../../stores/notice';
import NoticeModal from './_notice';

export default class Notice extends Reflux.Component {
  constructor(props) {
    super(props);
    this.stores = [NoticeStore];
    this.storeKeys = ['notice', 'show'];
    this.toggleModal = this.toggleModal.bind(this);
  }

  toggleModal () {
    const newState = _.extend(this.state, {show: !this.state.show});
    this.setState(newState);
  }

  render() {
    const notice = this.state.notice;
    return (
        <NoticeModal
          className='notice modal'
          show={this.state.show}
          onClose={this.toggleModal}
          notice={notice} />
    );
  }
}
