import Reflux from 'reflux';
import React from 'react';

import Nav from './nav';
import Title from './title';
import Notice from './notice';

export default class Header extends Reflux.Component {
  render() {
    return (
      <header className='header'>
        <Title title='Demo Reflux Client'/>
        <Notice/>
        <Nav/>
      </header>
    );
  }
}
