import React from 'react';

export default class FourZeroFour extends React.Component {
  render () {
    return(
      <div>
        <p>Error 404: Not Found</p>
      </div>
    );
  }
}
