import React from 'react';
import Reflux from 'reflux';
import { Link } from 'react-router-dom';
import { Nav, NavItem, NavDropdown, MenuItem } from 'react-bootstrap';

import CurrentUserActions from '../../actions/current_user';
import CurrentUsersStore from '../../stores/current_user';

export default class Header extends Reflux.Component {
  constructor (props) {
    super(props);
    this.stores = [CurrentUsersStore];
    this.storeKeys = ['current_user'];
    this.state = ''
    this.handleSignOut = this.handleSignOut.bind(this);
  }

  handleSignOut (event) {
    event.preventDefault();
    CurrentUserActions.signOut();
  }

  toggleSignInGreeting (props) {
    return !!this.state.current_user ? this.getUsernameOrEmail() :this.signInLink();
  }

  toggleSignOut (props) {
    if (!!this.state.current_user) {
      return (
        <MenuItem eventKey="3.1">
          {this.signOutLink()}
        </MenuItem>
      );
    } else {
      return false;
    }
  }

  signInLink () {
    return(
      <Link to="/sign_in">Sign In</Link>
    );
  }

  signOutLink () {
    return(
      <Link to="/sign_out" onClick={this.handleSignOut}>Sign Out</Link>
    );
  }

  getUsernameOrEmail () {
    return this.state.current_user.username || this.state.current_user.email;
  }

  newPostLink () {
    return (<MenuItem eventKey="3.2"><Link to="/post/new">New Post</Link></MenuItem>);
  }

  render () {
    return(
      <Nav bsStyle="tabs">
        <NavItem eventKey="1">
          <Link to="/">Home</Link>
        </NavItem>
        <NavItem eventKey="2" title="Item">
          <Link to="/posts">Posts</Link>
        </NavItem>
        <NavDropdown eventKey="3" title={this.toggleSignInGreeting()} id="nav-dropdown">
          {!!this.state.current_user ? this.newPostLink() : null}
          {!!this.state.current_user ? <MenuItem divider /> : null}
          {this.toggleSignOut()}
        </NavDropdown>
      </Nav>
    );
  }
}
