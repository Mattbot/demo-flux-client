// Libraries:
import React from 'react';
import { Col, Row, Media } from 'react-bootstrap';


const Home = () => (
  <div className='home'>
    <Col>
      <Row>
        <Media>
          <Media.Body>
            <h2>Introduction</h2>
            <p>
              <a href='https://gitlab.com/Mattbot/demo-flux-client'>This code</a> is a
              demonstration of a single page ES6 Javascript web application
              built using <a href='https://github.com/reflux/refluxjs/blob/master/docs/other/reflux-vs-flux.md'>
              Reflux</a> architecture and rendered via <a href='https://reactjs.org/'>React</a>. It
              implements a very simple blog site which is back-ended by
              two <a href='https://rubyonrails.org/'>Ruby on Rails</a> based microservices:
              a <a href='https://gitlab.com/Mattbot/demo-blog-api'>posts API</a> and
              a <a href='https://jwt.io/'>JWT token</a> based
              authentication <a href='https://gitlab.com/Mattbot/demo-jwt-api'>user API</a>.
            </p>
              <p>This site is a part of a larger <a href='https://gitlab.com/Mattbot/demo-infrastructure'>full stack demonstratration</a>. This
              blog and the two API microservices are running in <a href='https://www.docker.com/'>Docker</a> containers
              hosted on an Amazon <a href='https://aws.amazon.com/ecs/'>AWS Elastic Container Service</a> cluster.
              Docker containers are used from development to deployment to
              maintain <a href='https://convox.com/blog/dev-test-prod-parity/'>environment parity</a> in
              accordence with the <a href='https://12factor.net'>12 Factor</a> approach to web development.
              The entirety of the AWS infrastructure is built and maintained
              using <a href='https://www.terraform.io/'>Terraform</a> and follows the pricipals
              of <a href='https://en.wikipedia.org/wiki/Infrastructure_as_Code'>Infrastructure as Code</a> as well
              as <a href='https://www.oreilly.com/ideas/an-introduction-to-immutable-infrastructure'>Immutable Architecture</a>.  The infrastructure includes load balancing, auto-scaling, content delivery
              networks and all the other technologies required for a modern, high traffic, production ready back end.
            </p>
            <h2>Purpose</h2>
            <p>
              The purpose of creating this collection of web application technology demos
              is to consolidate my thoughts, experiences, and hard-won best practices
              relating to my work as a professional full stack web application developer
              and DevOps specialist. I hope this showcase will aid potential clients in
              assessing my philosophies and my experience level better than the typical
              hit-or-miss processes used in traditional interview settings.
            </p>
            <p>- <a href='https://resume.mattbot.net'>Matt Ridenour, June 2018</a></p>
          </Media.Body>
        </Media>
      </Row>
    </Col>
  </div>
);

export default Home;
