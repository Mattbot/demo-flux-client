import React from 'react';
import ReactDOM from 'react-dom';
import registerServiceWorker from './lib/registerServiceWorker';

import { Router } from 'react-router-dom';

import './stylesheets/index.css';
import Setup from './lib/setup';
import History from './lib/history';

import App from './components/app/app';

Setup.run();
const history = History;

ReactDOM.render(
  <Router history={history}>
    <App />
  </Router>,
  document.getElementById('root')
);

registerServiceWorker();
