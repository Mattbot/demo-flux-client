import Reflux from 'reflux';
import _ from 'lodash';

import AppStore from '../stores/app';
import CurrentPostStore from '../stores/current_post';
import CurrentUserStore from '../stores/current_user';
import UsersStore from '../stores/users';
import PostsStore from '../stores/posts';

export default class Setup {

  static stores = {
      app: AppStore,
      current_post: CurrentPostStore,
      current_user: CurrentUserStore,
      users: UsersStore,
      posts: PostsStore
    }

  static run () {
    console.log('Initializing stores...')
    this.initStores();
  }

  static initStores () {
    _.keys(this.stores).forEach( (storeName) => {
      Reflux.initStore(this.stores[storeName]);
    });
  }
}
