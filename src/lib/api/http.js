export default class HTTP {
  static postRequest(url, data, jwt_auth = null) {
    return new Promise(function (resolve, reject) {
      const response = '';
      const xhr = new XMLHttpRequest();
      xhr.open("POST", url, true);
      xhr.responseType = 'json';
      xhr.setRequestHeader('Content-type','application/json; charset=utf-8');
      if (!!jwt_auth){
        xhr.setRequestHeader("Authorization", "Bearer " + jwt_auth);
      }
      xhr.onload = function() {
        if (this.readyState === 4 && this.status === 201) {
          resolve(this.response);
        } else {
          reject({
            status: this.status,
            statusText: this.statusText
          });
          console.error(`ERROR > HTTP.postRequest: ${response}`);
        }
      };
      xhr.onerror = function () {
        reject({
          status: this.status,
          statusText: this.statusText
        });
      };
      xhr.send(JSON.stringify(data));
    });
  }

  static getRequest(url, data) {
    return new Promise(function (resolve, reject) {
      const response = '';
      const xhr = new XMLHttpRequest();
      xhr.open("GET", url, true);
      xhr.responseType = 'json';
      xhr.onload = function() {
        if (this.readyState === 4 && this.status === 200) {
          resolve(this.response);
        } else {
          reject({
            status: this.status,
            statusText: this.statusText
          });
          console.error(`ERROR > HTTP.getRequest: ${response}`);
        }
      };
      xhr.onerror = function () {
        reject({
          status: this.status,
          statusText: this.statusText
        });
      };
      xhr.send(data);
    });
  }

  static putRequest(url, data, jwt_auth = null) {
    return new Promise(function (resolve, reject) {
      const response = '';
      const xhr = new XMLHttpRequest();
      xhr.open("PUT", url, true);
      xhr.responseType = 'json';
      xhr.setRequestHeader('Content-type','application/json; charset=utf-8');
      if (!!jwt_auth){
        xhr.setRequestHeader("Authorization", "Bearer " + jwt_auth);
      }
      xhr.onload = function() {
        if (this.readyState === 4 && this.status === 200) {
          resolve(this.response);
        } else {
          reject({
            status: this.status,
            statusText: this.statusText
          });
          console.error(`ERROR > HTTP.putRequest: ${response}`);
        }
      };
      xhr.onerror = function () {
        reject({
          status: this.status,
          statusText: this.statusText
        });
      };
      xhr.send(JSON.stringify(data));
    });
  }

  static deleteRequest(url, data, jwt_auth = null) {
    return new Promise(function (resolve, reject) {
      const response = '';
      const xhr = new XMLHttpRequest();
      xhr.open("DELETE", url, true);
      xhr.responseType = 'json';
      if (!!jwt_auth){
        xhr.setRequestHeader("Authorization", "Bearer " + jwt_auth);
      }
      xhr.onload = function() {
        if (this.readyState === 4 && this.status === 204) {
          resolve(this.response);
        } else {
          reject({
            status: this.status,
            statusText: this.statusText
          });
          console.error(`ERROR > HTTP.deleteRequest: ${response}`);
        }
      };
      xhr.onerror = function () {
        reject({
          status: this.status,
          statusText: this.statusText
        });
      };
      xhr.send(data);
    });
  }

}
