export default class Config {
  static get USER_API_BASE_URL() { return process.env.REACT_APP_USER_API_BASE_URL };
  static get POST_API_BASE_URL() { return process.env.REACT_APP_POST_API_BASE_URL };
}
