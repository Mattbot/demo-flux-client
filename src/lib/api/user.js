import Config from './config';
import HTTP from './http';

export default class UserApi {
  static signIn (params) {
    const url = this.signInEndpoint();
    return HTTP.postRequest(url, params)
  }

  static all (params = '') {
    const url = this.allEndpoint();
    return HTTP.getRequest(url, params)
  }

  static find (id) {
    const url = this.findEndpoint() + id;
    return HTTP.getRequest(url, null);
  }

  static userApiBaseUrl () {
    return Config.USER_API_BASE_URL;
  }

  static signInEndpoint () {
    return `${this.userApiBaseUrl()}/api/v1/api_user_token`;
  }

  static allEndpoint () {
    return `${this.userApiBaseUrl()}/api/v1/api_users`;
  }

  static findEndpoint () {
    return `${this.userApiBaseUrl()}/api/v1/api_users/`;
  }
}
