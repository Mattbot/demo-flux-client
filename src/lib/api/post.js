import Config from './config';
import HTTP from './http';

export default class PostApi {
  static all (params = '') {
    const url = this.allEndpoint();
    return HTTP.getRequest(url, params)
  }

  static find (id) {
    const url = this.findEndpoint() + id;
    return HTTP.getRequest(url, null);
  }

  static new (params, jwt_token) {
    const url = this.newEndpoint();
    return HTTP.postRequest(url, params, jwt_token);
  }

  static edit (params, jwt_token) {
    const url = this.editEndpoint() + params.id;
    return HTTP.putRequest(url, params, jwt_token);
  }

  static delete (params, jwt_token) {
    const url = this.deleteEndpoint() + params.id;
    return HTTP.deleteRequest(url, params, jwt_token);
  }

  static postApiBaseUrl () {
    return Config.POST_API_BASE_URL;
  }

  static allEndpoint () {
    return `${this.postApiBaseUrl()}/api/v1/posts`;
  }

  static findEndpoint () {
    return `${this.postApiBaseUrl()}/api/v1/posts/`;
  }

  static newEndpoint () {
    return `${this.postApiBaseUrl()}/api/v1/posts`;
  }

  static editEndpoint () {
    return `${this.postApiBaseUrl()}/api/v1/posts/`;
  }

  static deleteEndpoint () {
    return `${this.postApiBaseUrl()}/api/v1/posts/`;
  }
}
