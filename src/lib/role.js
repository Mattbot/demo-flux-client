export default class Role
{
  static isAuthor (current_user, post) {
    if (!!current_user && !!post) {
      return current_user.id === post.get('user_id') ? true : false;
    } else {
      return false;
    }
  }
}
