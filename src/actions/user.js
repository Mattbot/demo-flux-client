import BaseActions from './base';
import UserApi from '../lib/api/user';

const UserActions = BaseActions('User');

UserActions.allUser.listen( async () => {
  try {
    const response = await UserApi.all();
    UserActions.allUserCompleted(response);
  } catch (error) {
    UserActions.allUserFailed(error);
  }
});

UserActions.findUser.listen( async (id) => {
  try {
    const response = await UserApi.find(id);
    UserActions.findUserCompleted(response);
  } catch (error) {
    UserActions.findUserFailed(error);
  }
});

export default UserActions;


