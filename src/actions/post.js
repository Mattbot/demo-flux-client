import BaseActions from './base';
import PostApi from '../lib/api/post';
import History from '../lib/history';

const PostActions = BaseActions('Post');

PostActions.allPost.listen( async () => {
  try {
    const response = await PostApi.all();
    PostActions.allPostCompleted(response);
  } catch (error) {
    PostActions.allPostFailed(error);
  }
});

PostActions.findPost.listen( async (id) => {
  try {
    var response = await PostApi.find(id);
    PostActions.findPostCompleted(response);
  } catch (error) {
    PostActions.findPostFailed(error);
  }
  return response;
});

PostActions.newPost.listen( async (user, post) => {
  const params = { user_id: user.id, title: post.title , body: post.body };
  try {
    const response = await PostApi.new(params, user.token);
    PostActions.newPostCompleted(response);
  } catch (error) {
    PostActions.newPostFailed(error);
  }
});

PostActions.editPost.listen( async (user, post) => {
  const params = { user_id: post.user_id, id: post.id, title: post.title , body: post.body };
  try {
    const response = await PostApi.edit(params, user.token);
    PostActions.editPostCompleted(response);
    History.push(`/post/${post.id}`);
  } catch (error) {
    PostActions.editPostFailed(error);
  }
});

PostActions.deletePost.listen( async (user, post) => {
  const params = { id: post.get('id') };
  try {
    const response = await PostApi.delete(params, user.token);
    PostActions.deletePostCompleted(response);
    History.push('/posts');
  } catch (error) {
    PostActions.deletePostFailed(error);
  }
});

export default PostActions;
