import _ from 'lodash';
import Reflux from 'reflux';

function baseActions (storeName) {
  const actionMap = createActionMap(storeName);
  return (Reflux.createActions(actionMap));
}

function createActionMap (storeName) {
  const normalizedStoreName = _.capitalize(storeName);
  const actions = ['all', 'find', 'new', 'edit', 'delete'];
  const childActions = ['Completed', 'Failed'];
  let actionMap = {};
  _.each(actions, (action) => {
    actionMap[action + normalizedStoreName] = {};
    _.each(childActions, (childAction) => {
      actionMap[action + normalizedStoreName + childAction] = {};
    });
  });
  return actionMap;
}

export default baseActions;
