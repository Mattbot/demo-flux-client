import Reflux from 'reflux';

import UserApi from '../lib/api/user';

const CurrentUserActions = Reflux.createActions([
  { 'signIn': {} },
  { 'signInCompleted': {} },
  { 'signInFailed': {} },
  { 'signOut': { sync: true } }
]);

export default CurrentUserActions;

CurrentUserActions.signIn.listen( async (email, password) => {
  const params = { auth: { email, password } };
  try {
    const response = await UserApi.signIn(params);
    CurrentUserActions.signInCompleted(response);
  } catch (error) {
    CurrentUserActions.signInFailed(error);
  }
});
