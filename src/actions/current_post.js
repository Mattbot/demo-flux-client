import Reflux from 'reflux';

import UserActions from '../actions/user';

const CurrentPostActions = Reflux.createActions([
  { 'getUsername': {} },
  { 'getUsernameCompleted': {} },
  { 'getUsernameFailed': {} }
]);

export default CurrentPostActions;

CurrentPostActions.getUsername.listen( async (id) => {
  try {
    const user = await UserActions.findUser(id);
    CurrentPostActions.getUsernameCompleted(user);
  } catch (error) {
    CurrentPostActions.getUsernameFailed(error);
  }
});
