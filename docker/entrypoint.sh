#!/bin/bash

set -euo pipefail

# This script is the ENTRYPOINT defined in the Dockerfile, and it's purpose is to prepare the environment for running the process in the container to use in the app.

report() {
  printf "[entrypoint.sh] %s\n" "$1" >&2
}

export() {
  for assign in "$@"; do
    builtin export -- "$assign"
    name="${assign%%=*}"
    value="$(eval "echo \$${name}")"
    [[ ${ENTRYPOINT_TRACE:-} ]] && report "${name}='${value}'" || :
  done
}

mkdir -p tmp/sockets log

report "exec $*"
exec $@

